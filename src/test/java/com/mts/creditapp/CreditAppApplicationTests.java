package com.mts.creditapp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mts.creditapp.entity.inputEntities.DeleteCreditInput;
import com.mts.creditapp.entity.inputEntities.NewCreditInput;
import com.mts.creditapp.entity.outputEnitites.outputs.GenericOutput;
import com.mts.creditapp.entity.outputEnitites.outputs.NewCreditOutput;
import com.mts.creditapp.service.CreditUserServices;
import org.json.JSONException;
import org.junit.jupiter.api.*;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class CreditAppApplicationTests {
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private CreditUserServices creditUserServices;
    private static HttpHeaders httpHeaders;
    private String addNewOrderUrl(){
        return "http://localhost:"+port+"/loan-service/order";
    }
    private String getOrderStatusUrl(UUID orderId){
        return "http://localhost:"+port+"/loan-service/getStatusOrder?orderId="+orderId.toString();
    }
    private String getAllTariffsUrl() {
        return "http://localhost:"+port+"/loan-service/getTariffs";
    }
    private String deleteOrderUrl() {
        return "http://localhost:"+port+"/loan-service/deleteOrder";
    }
    private <T> String toJson(T t) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(t);
    }
    @BeforeAll
    public static void init() {
        httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    }
    @Test
    public void testGetAllTariffs() throws JSONException {
        String expectedOutput = "{" +
                "        \"tariffs\": [" +
                "            {" +
                "                \"id\": 1,\n" +
                "                \"type\": \"BUSINESS\"," +
                "                \"interestRate\": \"2%\"" +
                "            }," +
                "            {" +
                "                \"id\": 2," +
                "                \"type\": \"CONSUMER\"," +
                "                \"interestRate\": \"10%\"" +
                "            }," +
                "            {" +
                "                \"id\": 3," +
                "                \"type\": \"RETIRED\"," +
                "                \"interestRate\": \"4%\"" +
                "            }" +
                "        ]" +
                "}";
        ResponseEntity<GenericOutput> response =
                restTemplate.getForEntity(getAllTariffsUrl(), GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getData());
        JSONAssert.assertEquals(
                expectedOutput,
                String.valueOf(response.getBody().getData()),
                JSONCompareMode.STRICT);
    }
    @Test
    public void testAddNewOrder() throws JsonProcessingException, InterruptedException {
        long testUserId = (long) (Math.random()*10000);
        long testTariffId = 4;
        NewCreditInput testOrderInput = new NewCreditInput(testUserId,testTariffId);
        HttpEntity<String> entity = new HttpEntity<>(toJson(testOrderInput), httpHeaders);
        ResponseEntity<GenericOutput> response =
                restTemplate.exchange(addNewOrderUrl(), HttpMethod.POST, entity, GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
        testTariffId=1;
        testOrderInput = new NewCreditInput(testUserId,testTariffId);
        entity = new HttpEntity<>(toJson(testOrderInput), httpHeaders);
        response = restTemplate.exchange(addNewOrderUrl(), HttpMethod.POST, entity, GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getData());
        response = restTemplate.exchange(addNewOrderUrl(), HttpMethod.POST, entity, GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
        Thread.sleep(120000);
        response = restTemplate.exchange(addNewOrderUrl(), HttpMethod.POST, entity, GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
    }
    @Test
    public void testGetOrderStatus() throws JSONException {
        long testUserId = (long) (Math.random()*10000);
        long testTariffId = 1;
        String expectedOutput = "{\"orderStatus\": \"IN_PROGRESS\"}";
        UUID testOrderId = UUID.randomUUID();
        ResponseEntity<GenericOutput> response =
                restTemplate.getForEntity(getOrderStatusUrl(testOrderId), GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
        NewCreditOutput testCreditOutput =
                (NewCreditOutput) creditUserServices.newCredit(new NewCreditInput(testUserId,testTariffId)).getData();
        testOrderId = testCreditOutput.getOrderId();
        response = restTemplate.getForEntity(getOrderStatusUrl(testOrderId), GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertNotNull(response.getBody());
        JSONAssert.assertEquals(
                expectedOutput,
                String.valueOf(response.getBody().getData()),
                JSONCompareMode.STRICT);
    }

    @Test
    public void testDeleteOrder() throws JsonProcessingException, InterruptedException {
        long testUserId = (long) (Math.random()*10000);
        long testTariffId = 1;
        UUID testOrderId = UUID.randomUUID();
        DeleteCreditInput testDeleteCreditInput = new DeleteCreditInput(testUserId, testOrderId);
        HttpEntity<String> entity = new HttpEntity<>(toJson(testDeleteCreditInput), httpHeaders);
        ResponseEntity<GenericOutput> response =
                restTemplate.exchange(deleteOrderUrl(), HttpMethod.DELETE, entity, GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
        NewCreditOutput testCreditOutput =
                (NewCreditOutput) creditUserServices.newCredit(new NewCreditInput(testUserId,testTariffId)).getData();
        testOrderId = testCreditOutput.getOrderId();
        testDeleteCreditInput = new DeleteCreditInput(testUserId, testOrderId);
        entity = new HttpEntity<>(toJson(testDeleteCreditInput), httpHeaders);
        response = restTemplate.exchange(deleteOrderUrl(), HttpMethod.DELETE, entity, GenericOutput.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(400));
        Thread.sleep(120000);
        ResponseEntity<Void> responseReadyToDelete =
                restTemplate.exchange(deleteOrderUrl(), HttpMethod.DELETE, entity, Void.class);
        assertThat(responseReadyToDelete.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
